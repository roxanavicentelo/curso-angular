import { Component, OnInit, Input, HostBinding} from '@angular/core';
import { Pelicula } from '../models/pelicula.model';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {
  @Input()peli:Pelicula;
  @HostBinding("attr.class") cssClass=' col-md-4  mt-4';
  constructor() { }

  ngOnInit(): void {
  }

}
