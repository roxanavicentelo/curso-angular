import { Component, OnInit } from '@angular/core';
import { Pelicula } from '../models/pelicula.model';

@Component({
  selector: 'app-lista-pelicula',
  templateUrl: './lista-pelicula.component.html',
  styleUrls: ['./lista-pelicula.component.css']
})
export class ListaPeliculaComponent implements OnInit {
  peliculas:Pelicula[];
  constructor() {
    this.peliculas=[];
  }

  ngOnInit(): void {
  }
  guardar(nombre:string,url:string):boolean{
    this.peliculas.push(new Pelicula(nombre,url));
    console.log(this.peliculas);
    (<HTMLInputElement>document.getElementById("nombre")).value="";
    (<HTMLInputElement>document.getElementById("imagenUrl")).value="";
    return false;
  }

}
