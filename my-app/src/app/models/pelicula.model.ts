export class Pelicula {
  nombre: string;
  imagenUrl: string;
  constructor(n: string, img: string) {
    this.nombre = n;
    this.imagenUrl = img;
  }
}
